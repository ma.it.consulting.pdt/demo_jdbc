package org.example.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Callable;

public class ConnectionUtils {


    public static Connection getMySQLConnection() throws SQLException {

        String url = "jdbc:mysql://localhost:3306/bd_mysql?useSSL=false";

        Connection connection = DriverManager.getConnection(url,"root","");

        return connection;

        };



}
