package org.example.domain;

import java.io.Serializable;

public class Department implements Serializable {

    private Integer depId;
    private String name;

    public Department(String name) {
        this.name = name;
    }

    public Department(){

    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Department{" +
                "depId=" + depId +
                ", name='" + name + '\'' +
                '}';
    }
}
